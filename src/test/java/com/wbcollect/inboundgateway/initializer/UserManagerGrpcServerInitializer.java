package com.wbcollect.inboundgateway.initializer;

import com.wbcollect.inboundgateway.controller.UserController;
import com.wbcollect.usermanager.UserManagerGrpc;
import com.wbcollect.usermanager.UserManagerProto;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UserManagerGrpcServerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    private final Server SERVER = ServerBuilder.forPort(0)
            .addService(getBindableService())
            .build();

    private final Map<String, UserController.UserAdapter> users = new HashMap<>();

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        try {
            SERVER.start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        TestPropertyValues.of(
                "com.wbcollect.common.service-urls.user-manager" + /*getServiceName() + */ "=" + "localhost:" + SERVER.getPort()
        ).applyTo(applicationContext);
    }

    protected UserManagerGrpc.UserManagerImplBase getBindableService() {
        return new UserManagerGrpc.UserManagerImplBase() {

            @Override
            public void getUser(UserManagerProto.UserRequest request, StreamObserver<UserManagerProto.UserReply> responseObserver) {
                var user = users.get(request.getEmail());
                if (user == null) {
                    responseObserver.onError(new StatusRuntimeException(Status.NOT_FOUND));
                    return;
                }
                responseObserver.onNext(UserManagerProto.UserReply.newBuilder().setUser(mapUser(user)).build());
                responseObserver.onCompleted();
            }

            @Override
            public void createUser(UserManagerProto.CreateUserRequest request, StreamObserver<UserManagerProto.CreateUserReply> responseObserver) {
                UserController.UserAdapter entity = new UserController.UserAdapter(
                        System.currentTimeMillis(),
                        request.getUsername(),
                        request.getPassword(),
                        request.getEmail(),
                        "NEW");
                users.put(entity.getEmail(), entity);
                var user = mapUser(entity);
                responseObserver.onNext(UserManagerProto.CreateUserReply.newBuilder().setUser(user).build());
                responseObserver.onCompleted();
            }

            @Override
            public void changeUser(UserManagerProto.ChangeUserRequest request, StreamObserver<UserManagerProto.ChangeUserReply> responseObserver) {
                var user = users.get(request.getEmail());
                if (user == null) {
                    responseObserver.onError(new StatusRuntimeException(Status.NOT_FOUND));
                    return;
                }
                var newUsername = request.getUsername();
                var newPassword = request.getPassword();
                var newStatus = request.getStatus();
                if (!"".equals(newUsername))
                    user.setUsername(newUsername);
                if (!"".equals(newPassword))
                    user.setPassword(newPassword);
                if (!"".equals(newStatus))
                    user.setStatus(newStatus);
                users.put(user.getEmail(), user);
                responseObserver.onNext(UserManagerProto.ChangeUserReply.newBuilder().setUser(mapUser(user)).build());
                responseObserver.onCompleted();
            }
        };
    }

    private static UserManagerProto.User mapUser(UserController.UserAdapter entity) {
        return UserManagerProto.User.newBuilder()
                .setId(entity.getId())
                .setUsername(entity.getUsername())
                .setPassword(entity.getPassword())
                .setEmail(entity.getEmail())
                .setStatus(entity.getStatus())
                .build();
    }
}
