package com.wbcollect.inboundgateway.initializer;

import com.google.protobuf.Timestamp;
import com.wbcollect.statistics.StatisticsGrpc;
import com.wbcollect.statistics.StatisticsProto;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class StatisticsGrpcServerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    private final Server SERVER = ServerBuilder.forPort(0)
            .addService(getBindableService())
            .build();


    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        try {
            SERVER.start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        TestPropertyValues.of(
                "com.wbcollect.common.service-urls.statistics" + "=" + "localhost:" + SERVER.getPort()
        ).applyTo(applicationContext);
    }

    protected StatisticsGrpc.StatisticsImplBase getBindableService() {
        return new StatisticsGrpc.StatisticsImplBase() {
            @Override
            public void getProductStats(StatisticsProto.ProductStatsRequest request, StreamObserver<StatisticsProto.ProductStatsReply> responseObserver) {
                var baseDate = Instant.parse("2023-05-07T00:00:00.00Z");
                List<StatisticsProto.ProductStats> productStats = new ArrayList<>();
                for (int i = 0; i < request.getNumberOfDays(); i++) {

                    productStats.add(StatisticsProto.ProductStats.newBuilder()
                            .setDate(Timestamp.newBuilder()
                                    .setSeconds(baseDate.getEpochSecond())
                                    .setNanos(baseDate.getNano())
                                    .build())
                            .setSales(5)
                            .setQuantity(100)
                            .setPrice(500)
                            .setRatingPercentage(50)
                            .build()
                    );
                    baseDate.plus(Duration.ofDays(1));
                }
                responseObserver.onNext(StatisticsProto.ProductStatsReply.newBuilder()
                        .addAllProductStats(productStats)
                        .build());
                responseObserver.onCompleted();
            }

            @Override
            public void getProductInfo(StatisticsProto.ProductInfoRequest request, StreamObserver<StatisticsProto.ProductInfoReply> responseObserver) {
                var date = Instant.parse("2023-05-07T00:00:00.00Z");
                var reply = StatisticsProto.ProductInfoReply.newBuilder()
                        .setProductInfo(StatisticsProto.ProductInfo.newBuilder()
                                .setId(1L)
                                .setName("t-shirt")
                                .setBrandName("adidas")
                                .setPrice(100)
                                .setPriceWithSale(60)
                                .setDate(Timestamp.newBuilder()
                                        .setSeconds(date.getEpochSecond())
                                        .setNanos(date.getNano())
                                        .build()
                                )
                                .build()
                        )
                        .setStatus(200)
                        .build();

                responseObserver.onNext(reply);
                responseObserver.onCompleted();
            }

            @Override
            public void getCategoryStats(StatisticsProto.CategoryStatsRequest request, StreamObserver<StatisticsProto.CategoryStatsReply> responseObserver) {
                var baseDate = Instant.parse("2023-05-07T00:00:00.00Z");
                List<StatisticsProto.CategoryStats> categoryStats = new ArrayList<>();
                for (int i = 0; i < request.getNumberOfDays(); i++) {

                    categoryStats.add(StatisticsProto.CategoryStats.newBuilder()
                            .setDate(Timestamp.newBuilder()
                                    .setSeconds(baseDate.getEpochSecond())
                                    .setNanos(baseDate.getNano())
                                    .build())
                            .setSales(i)
                            .setRevenue(i * 100)
                            .build()
                    );
                    baseDate.plus(Duration.ofDays(1));
                }
                responseObserver.onNext(StatisticsProto.CategoryStatsReply.newBuilder()
                        .addAllCategoryStats(categoryStats)
                        .build());
                responseObserver.onCompleted();
            }
        };
    }
}
