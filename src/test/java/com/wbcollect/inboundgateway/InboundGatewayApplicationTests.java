package com.wbcollect.inboundgateway;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wbcollect.inboundgateway.config.SpringSecurityWebAuxTestConfig;
import com.wbcollect.inboundgateway.controller.CategoryStatisticsController;
import com.wbcollect.inboundgateway.controller.ProductStatisticsController;
import com.wbcollect.inboundgateway.controller.UserController;
import com.wbcollect.inboundgateway.initializer.StatisticsGrpcServerInitializer;
import com.wbcollect.inboundgateway.initializer.UserManagerGrpcServerInitializer;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockserver.springtest.MockServerTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@MockServerTest
@ContextConfiguration(
        initializers = {
                UserManagerGrpcServerInitializer.class,
                StatisticsGrpcServerInitializer.class,
        })
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = SpringSecurityWebAuxTestConfig.class
)
@TestPropertySource(
        properties = {"spring.main.allow-bean-definition-overriding=true"}
)
class InboundGatewayApplicationTests {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    /**
     * CREATE USER TESTS
     */

    @Test
    @WithAnonymousUser
    public void test_createUserConfirmPasswordNotMatches_shouldReturn400() throws Exception {
        var username = "firstName2";
        var password = "Password1";
        var confirmPassword = "Password2";
        var email = "user@company.com";

        // performing create user request and saving result
        var body = mockMvc.perform(MockMvcRequestBuilders
                        .post("/users/create")
                        .content(asJsonString(new UserController.UserAdapter(
                                System.currentTimeMillis(),  // create constructor without id or leave as is?
                                username, password, confirmPassword, email, "NEW")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithAnonymousUser
    public void test_createUserPasswordNotValid_shouldReturn400() throws Exception {
        var username = "firstName2";
        var password = "password1";
        var confirmPassword = "password1";
        var email = "user@company.com";

        // performing create user request and saving result
        var body = mockMvc.perform(MockMvcRequestBuilders
                        .post("/users/create")
                        .content(asJsonString(new UserController.UserAdapter(
                                System.currentTimeMillis(),  // create constructor without id or leave as is?
                                username, password, confirmPassword, email, "NEW")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithAnonymousUser
    public void test_createUser_shouldReturn200() throws Exception {
        var username = "firstName2";
        var password = "Password1";
        var confirmPassword = "Password1";
        var email = "user@company.com";

        // performing create user request and saving result
        var body = mockMvc.perform(MockMvcRequestBuilders
                        .post("/users/create")
                        .content(asJsonString(new UserController.UserAdapter(
                                System.currentTimeMillis(),  // create constructor without id or leave as is?
                                username, password, confirmPassword, email, "NEW")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        // checking each field
        UserController.UserAdapter returnedUser = parseJson(body, UserController.UserAdapter.class);
        assertThat(returnedUser.getId()).isGreaterThan(0L);
        assertEquals(username, returnedUser.getUsername());
        assertEquals(password, returnedUser.getPassword());
        assertEquals(email, returnedUser.getEmail());
    }

    /**
     * GET USER TESTS
     */

    @Test
    @WithAnonymousUser
    public void test_getUserByNotAuthenticatedUser_shouldReturn401() throws Exception {

        var email = "user@company.com";
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/users/get-info-by-email/{email}", email)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails("user@company.com")
    public void test_getNotCreatedUser_shouldReturn404() throws Exception {

        var email = "user@company.com";
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/users/get-info-by-email/{email}", email)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithAnonymousUser
    public void test_createAndGetUser_shouldReturn200() throws Exception {
        var username = "firstName2";
        var password = "Password1";
        var confirmPassword = "Password1";
        var email = "testuser@company.com";

        // performing create user request and saving result
        var body = mockMvc.perform(MockMvcRequestBuilders
                        .post("/users/create")
                        .content(asJsonString(new UserController.UserAdapter(
                                System.currentTimeMillis(),
                                username, password, confirmPassword, email, "NEW")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserController.UserAdapter returnedUser = parseJson(body, UserController.UserAdapter.class);

        // trying to get just created user by email from previously returned body
        body = mockMvc.perform(MockMvcRequestBuilders
                        .get("/users/get-info-by-email/{email}", returnedUser.getEmail())
                        .with(httpBasic(returnedUser.getEmail(), returnedUser.getPassword()))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        // checking the result of getUser by email
        returnedUser = parseJson(body, UserController.UserAdapter.class);
        assertEquals(username, returnedUser.getUsername());
        assertEquals(password, returnedUser.getPassword());
        assertEquals(email, returnedUser.getEmail());
    }

    /**
     * CHANGE USER TESTS
     */

    @Test
    @WithAnonymousUser
    public void test_createAndChangeUserThenGetUserAndCheckChanges_shouldReturn200() throws Exception {
        var initUsername = "basic user";
        var newUsername = "cool user";
        var password = "Password1";
        var confirmPassword = "Password1";
        var newPassword = "Password12345";
        var confirmNewPassword = "Password12345";
        var email = "testuser@company.com";
        var newStatus = "SUBSCRIPTION_PAID";

        // performing create user request and saving result
        var body = mockMvc.perform(MockMvcRequestBuilders
                        .post("/users/create")
                        .content(asJsonString(new UserController.UserAdapter(
                                System.currentTimeMillis(),
                                initUsername, password, confirmPassword, email, "NEW")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserController.UserAdapter returnedUser = parseJson(body, UserController.UserAdapter.class);

        // trying to change name
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/users/change-username")
                        .with(httpBasic(returnedUser.getEmail(), returnedUser.getPassword()))
                        .content(asJsonString(new UserController.ChangeUserNameAdapter(returnedUser.getEmail(), newUsername)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


		// trying to change status
		mockMvc.perform(MockMvcRequestBuilders
						.put("/users/change-status")
						.with(httpBasic(returnedUser.getEmail(), returnedUser.getPassword()))
						.content(asJsonString(new UserController.ChangeUserStatusAdapter(returnedUser.getEmail(),
								newStatus)))
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

        // trying to change status
		mockMvc.perform(MockMvcRequestBuilders
						.put("/users/change-status")
						.with(httpBasic(returnedUser.getEmail(), returnedUser.getPassword()))
						.content(asJsonString(new UserController.ChangeUserStatusAdapter(returnedUser.getEmail(),
								newStatus)))
						.contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());// trying to change password with wrong old password
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/users/change-password")
                        .with(httpBasic(returnedUser.getEmail(), returnedUser.getPassword()))
                        .content(asJsonString(new UserController.ChangeUserPasswordAdapter(returnedUser.getEmail(),
                                "wrong old password",
                                newPassword,
                                confirmNewPassword)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        // trying to change password
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/users/change-password")
                        .with(httpBasic(returnedUser.getEmail(), returnedUser.getPassword()))
                        .content(asJsonString(new UserController.ChangeUserPasswordAdapter(returnedUser.getEmail(),
                                password,
                                newPassword,
                                confirmNewPassword)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // getting user with new name
        body = mockMvc.perform(MockMvcRequestBuilders
                        .get("/users/get-info-by-email/{email}", returnedUser.getEmail())
                        .with(httpBasic(returnedUser.getEmail(), newPassword))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        // comparing result with expected new name
        returnedUser = parseJson(body, UserController.UserAdapter.class);
        assertEquals(email, returnedUser.getEmail());
        assertEquals(newPassword, returnedUser.getPassword());
        assertEquals(newUsername, returnedUser.getUsername());
		assertEquals(newStatus, returnedUser.getStatus());
    }

    @Test
    @WithAnonymousUser
    public void test_changeUserByNotAuthenticatedUser_shouldReturn401() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/users/change-username")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails("user@company.com")
    public void test_changeNotCreatedUser_shouldReturn404() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/users/change-username")
                        .content(asJsonString(new UserController.ChangeUserNameAdapter(
                                "notexistinguser@gmail.com",
                                "cool name")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /**
     * PREAUTHORIZE /statistics/content TESTS
     */

    @Test
    public void test_unauthorizedRequestOnPreauthorize_shouldReturn401() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/statistics/product/info/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithUserDetails("user@company.com")
    public void test_authorizedRequestWithoutRequiredRoleOnPreauthorize_shouldReturn403() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/statistics/product/info/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("subscriber@company.com")
    public void test_authorizedRequestToGetProductInfo_shouldSucceed() throws Exception {
        var body = mockMvc.perform(MockMvcRequestBuilders
                        .get("/statistics/product/info/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        var productInfo = parseJson(body, ProductStatisticsController.ProductInfoAdapter.class);
        assertEquals(1, productInfo.getId());
        assertEquals("t-shirt", productInfo.getName());
        assertEquals("adidas", productInfo.getBrandName());
        assertEquals(100, productInfo.getPrice());
        assertEquals(60, productInfo.getPriceWithSale());
        assertEquals(Instant.parse("2023-05-07T00:00:00.00Z"), productInfo.getDate().toInstant());
    }

    @Test
    @WithUserDetails("subscriber@company.com")
    public void test_authorizedRequestToGetProductStats_shouldSucceed() throws Exception {
        var body = mockMvc.perform(MockMvcRequestBuilders
                        .get("/statistics/product/stat/1?days=7")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        var productStatsArray = parseJson(body, ProductStatisticsController.ProductStatisticsAdapter.class).getStatisticList();
        assertEquals(7, productStatsArray.size());

        var productStats = productStatsArray.get(0);
        assertEquals(Instant.parse("2023-05-07T00:00:00.00Z"), productStats.getDate().toInstant());
        assertEquals(5, productStats.getSales());
        assertEquals(100, productStats.getQuantity());
        assertEquals(500, productStats.getPrice());
        assertEquals(50, productStats.getRatingPercentage());
    }

    @Test
    @WithUserDetails("subscriber@company.com")
    public void test_authorizedRequestToGetCategoryStats_shouldSucceed() throws Exception {
        var body = mockMvc.perform(MockMvcRequestBuilders
                        .get("/statistics/category/stat?days=7&url=/test/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        var categoryStatsArray = parseJson(body, CategoryStatisticsController.CategoryStatisticsAdapter.class).getStatisticList();
        assertEquals(7, categoryStatsArray.size());

        var categoryStats = categoryStatsArray.get(6);
        assertEquals(Instant.parse("2023-05-07T00:00:00.00Z"), categoryStats.getDate().toInstant());
        assertEquals(6, categoryStats.getSales());
        assertEquals(600, categoryStats.getRevenue());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T parseJson(final String str, final Class<T> valueType) {
        try {
            return new ObjectMapper().readValue(str, valueType);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
