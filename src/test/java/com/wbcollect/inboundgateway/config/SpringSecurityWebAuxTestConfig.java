package com.wbcollect.inboundgateway.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Arrays;
import java.util.List;

@TestConfiguration
public class SpringSecurityWebAuxTestConfig {

    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        User basicUser = new User(
                "user@company.com",
                "Password1",
                List.of(new SimpleGrantedAuthority("NEW"))
        );

        User subscribedUser = new User(
                "subscriber@company.com",
                "password",
                List.of(new SimpleGrantedAuthority("SUBSCRIPTION_PAID"))
        );

        return new InMemoryUserDetailsManager(Arrays.asList(
                basicUser, subscribedUser
        ));

    }


// Spring throws "Failed to load ApplicationContext" if added
    @Bean
    @Primary
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance() ;
    }
}
