package com.wbcollect.inboundgateway.security;

import com.wbcollect.inboundgateway.controller.UserController;
import com.wbcollect.usermanager.UserManagerGrpc;
import com.wbcollect.usermanager.UserManagerProto;
import io.grpc.StatusRuntimeException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class MyUserDetailsService implements UserDetailsService {
    private final UserManagerGrpc.UserManagerBlockingStub userManagerBlockingStub;

    // TODO: figure out how to properly work with usernames/emails
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        var userRequest = UserManagerProto.UserRequest.newBuilder().setEmail(email).build();

        try {
            var userReply = this.userManagerBlockingStub.getUser(userRequest);
            List<SimpleGrantedAuthority> authorities = List.of(new SimpleGrantedAuthority(userReply.getUser().getStatus()));
            return new User(userReply.getUser().getUsername(), userReply.getUser().getPassword(), authorities);

        } catch (final StatusRuntimeException e) {
            log.error("Request failed", e);
            throw new UsernameNotFoundException("User not found!");
        }
    }
}
