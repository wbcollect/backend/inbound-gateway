package com.wbcollect.inboundgateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "com.wbcollect.common")
public class ConfigClientProperties {
    private Map<String, String> serviceUrls = new HashMap<>();
    private Long grpcTimeoutMs;
}