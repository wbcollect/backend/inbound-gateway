package com.wbcollect.inboundgateway.config;

import com.wbcollect.common.GrpcAutoConfiguration;
import com.wbcollect.statistics.StatisticsGrpc;
import com.wbcollect.usermanager.UserManagerGrpc;
import io.grpc.ManagedChannel;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.wbcollect.common.grpc.StubFactory;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ImportAutoConfiguration({
//        GrpcServerAutoConfiguration.class, // Create required server beans
//        GrpcServerFactoryAutoConfiguration.class, // Select server implementation
//        GrpcClientAutoConfiguration.class
        GrpcAutoConfiguration.class
})
public class InboundGatewayConfiguration {
    @Autowired
    private ConfigClientProperties configClientProperties;

    @Bean
    public ManagedChannel userManagerChannel() {
        return NettyChannelBuilder.forTarget(resolveServiceName("user-manager"))
                .usePlaintext()
                .defaultLoadBalancingPolicy("round_robin")
                .build();
    }

    @Bean
    public ManagedChannel statisticsChannel() {
        return NettyChannelBuilder.forTarget(resolveServiceName("statistics"))
                .usePlaintext()
                .defaultLoadBalancingPolicy("round_robin")
                .build();
    }

    private String resolveServiceName(String serviceName) {
        return configClientProperties.getServiceUrls().computeIfAbsent(serviceName, key -> {
            throw new BeanInitializationException(
                    "Unable to resolve service URL for '" + key + "'. " +
                            "Consider to add " +
                            "'com.wbcollect.common.service-urls.user-manager=${<SERVICE_NAME_HOST>:localhost}:${<SERVICE_NAME_GRPC_PORT>:9090}' into your application.properties(.yaml) file.");
        });
    }

    @Bean
    public UserManagerGrpc.UserManagerBlockingStub userManagerBlockingStub(ManagedChannel userManagerChannel, StubFactory stubFactory) {
        return stubFactory.deadlinedStub(
                UserManagerGrpc.UserManagerBlockingStub.class,
                userManagerChannel);
    }

    @Bean
    public StatisticsGrpc.StatisticsBlockingStub statisticsBlockingStub(ManagedChannel statisticsChannel, StubFactory stubFactory) {
        return stubFactory.deadlinedStub(
                StatisticsGrpc.StatisticsBlockingStub.class,
                statisticsChannel);
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedMethods("GET","POST", "PUT", "DELETE", "OPTIONS")
                        .allowedHeaders("*");
            }
        };
    }
}
