package com.wbcollect.inboundgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class InboundGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(InboundGatewayApplication.class, args);
	}

}
