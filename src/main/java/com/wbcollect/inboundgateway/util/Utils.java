package com.wbcollect.inboundgateway.util;

import com.google.protobuf.Timestamp;
import com.wbcollect.inboundgateway.controller.CategoryStatisticsController;
import com.wbcollect.inboundgateway.controller.ProductStatisticsController;
import com.wbcollect.statistics.StatisticsProto;
import lombok.experimental.UtilityClass;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@UtilityClass
public class Utils {
    public ProductStatisticsController.ProductInfoAdapter mapProductInfoReplyToProductInfoAdapter(StatisticsProto.ProductInfoReply productInfoReply) {
        return new ProductStatisticsController.ProductInfoAdapter(
                productInfoReply.getProductInfo().getId(),
                productInfoReply.getProductInfo().getName(),
                productInfoReply.getProductInfo().getBrandName(),
                productInfoReply.getProductInfo().getPrice(),
                productInfoReply.getProductInfo().getPriceWithSale(),
                mapTimestampToDate(productInfoReply.getProductInfo().getDate())
        );
    }

    public Date mapTimestampToDate(Timestamp ts) {
        return Date.from(Instant.ofEpochSecond(ts.getSeconds(), ts.getNanos()));
    }

    public ProductStatisticsController.ProductStatisticsAdapter mapProductStatsReplyToProductStatisticsAdapter(StatisticsProto.ProductStatsReply productStatsReply) {
        List<ProductStatisticsController.ProductStatisticsAdapter.ProductStatistic> productStatisticList = new ArrayList<>();
        for (StatisticsProto.ProductStats productStats : productStatsReply.getProductStatsList()) {
            productStatisticList.add(mapProductStatsToProductStatistic(productStats));
        }

        return new ProductStatisticsController.ProductStatisticsAdapter(productStatisticList);
    }

    private static ProductStatisticsController.ProductStatisticsAdapter.ProductStatistic mapProductStatsToProductStatistic(StatisticsProto.ProductStats productStats) {
        return new ProductStatisticsController.ProductStatisticsAdapter.ProductStatistic(
                mapTimestampToDate(productStats.getDate()),
                productStats.getSales(),
                productStats.getQuantity(),
                productStats.getPrice(),
                productStats.getRatingPercentage()
        );
    }

    public CategoryStatisticsController.CategoryStatisticsAdapter mapCategoryStatsReplyToCategoryStatisticsAdapter(StatisticsProto.CategoryStatsReply reply) {
        List<CategoryStatisticsController.CategoryStatisticsAdapter.CategoryStatistic> categoryStatisticsList = new ArrayList<>();
        for (StatisticsProto.CategoryStats categoryStats : reply.getCategoryStatsList()) {
            categoryStatisticsList.add(mapCategoryStatsToCategoryStatistic(categoryStats));
        }
        return new CategoryStatisticsController.CategoryStatisticsAdapter(categoryStatisticsList);
    }

    private static CategoryStatisticsController.CategoryStatisticsAdapter.CategoryStatistic mapCategoryStatsToCategoryStatistic(StatisticsProto.CategoryStats categoryStats) {
        return new CategoryStatisticsController.CategoryStatisticsAdapter.CategoryStatistic(
                mapTimestampToDate(categoryStats.getDate()),
                categoryStats.getSales(),
                categoryStats.getRevenue()
        );
    }
}

