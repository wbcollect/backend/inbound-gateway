package com.wbcollect.inboundgateway.controller;

import com.wbcollect.inboundgateway.service.CategoryStatisticsService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/statistics/category")
public class CategoryStatisticsController {
    private final CategoryStatisticsService categoryStatisticsService;

    @PreAuthorize("hasAuthority('SUBSCRIPTION_PAID')")
    @GetMapping("/stat")
    public CategoryStatisticsAdapter getProductStatistic(@RequestParam("url") String url, @RequestParam("days") Integer days) {
        return categoryStatisticsService.getCategoryStatistics(url, days);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CategoryStatisticsAdapter {
        List<CategoryStatistic> statisticList;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public static class CategoryStatistic {
            Date date;

            Integer sales;

            Integer revenue;
        }
    }
}
