package com.wbcollect.inboundgateway.controller;

import com.wbcollect.inboundgateway.service.ProductStatisticsService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/statistics/product")
public class ProductStatisticsController {
    private final ProductStatisticsService productStatisticsService;

    @PreAuthorize("hasAuthority('SUBSCRIPTION_PAID')")
    @GetMapping("/info/{id}")
    public ProductStatisticsController.ProductInfoAdapter getProductInfo(@PathVariable("id") Long id) {
        return productStatisticsService.getProductInfo(id);
    }

    @PreAuthorize("hasAuthority('SUBSCRIPTION_PAID')")
    @GetMapping("/stat/{id}")
    public ProductStatisticsAdapter getProductStatistic(@PathVariable("id") Long id, @RequestParam("days") Integer days) {
        return productStatisticsService.getProductStatistics(id, days);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ProductStatisticsAdapter {
        List<ProductStatistic> statisticList;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public static class ProductStatistic {
            Date date;

            Integer sales;

            Integer quantity;

            Integer price;

            Integer ratingPercentage;
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ProductInfoAdapter {
        Long id;

        String name;

        String brandName;

        Integer price;

        Integer priceWithSale;

        Date date;
    }
}
