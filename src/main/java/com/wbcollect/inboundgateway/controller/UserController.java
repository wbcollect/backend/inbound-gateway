package com.wbcollect.inboundgateway.controller;

import com.wbcollect.inboundgateway.annotations.Password;
import com.wbcollect.inboundgateway.annotations.PasswordValueMatch;
import com.wbcollect.inboundgateway.service.UserService;
import lombok.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    @GetMapping("/get-info-by-email/{email}")
    public UserAdapter getUser(@PathVariable String email) {
        return userService.getUser(email);
    }

    @PostMapping("/create")
    public UserAdapter createUser(@Valid @RequestBody UserAdapter userAdapter) {
        return userService.createUser(userAdapter);
    }

    @PutMapping("/change-password")
    public UserAdapter changeUserPassword(@RequestBody @Valid ChangeUserPasswordAdapter data) {
        return userService.changeUserPassword(data.email, data.oldPassword, data.password);
    }

    @PutMapping("/change-username")
    public UserAdapter changeUserName(@RequestBody @Valid ChangeUserNameAdapter data) {
        return userService.changeUserName(data.email, data.newUsername);
    }

    @PutMapping("/change-status")
    public UserAdapter changeUserStatus(@RequestBody @Valid ChangeUserStatusAdapter userStatusAdapter) {
        return userService.changeUserStatus(userStatusAdapter.email, userStatusAdapter.status);
    }

    @PasswordValueMatch.List({
            @PasswordValueMatch(
                    field = "password",
                    fieldMatch = "confirmPassword",
                    message = "Passwords do not match!"
            )
    })
    @Data
    @AllArgsConstructor
    @RequiredArgsConstructor
    @NoArgsConstructor
    public static class UserAdapter {

        @NonNull
        Long id;

        @NonNull
        @NotBlank
        String username;

        @Password
        @NonNull
        @NotBlank
        String password;

        String confirmPassword;

        @Email
        @NonNull
        @NotBlank
        String email;

        @NonNull
        String status;
    }

    @PasswordValueMatch.List({
            @PasswordValueMatch(
                    field = "password",
                    fieldMatch = "confirmPassword",
                    message = "Passwords do not match!"
            )
    })
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ChangeUserPasswordAdapter {
        @NotBlank(message = "Should not be empty!")
        @Email
        String email;

        @NotBlank
        String oldPassword;
        @Password
        @NotBlank
        String password;

        String confirmPassword;

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ChangeUserNameAdapter {
        @NotBlank(message = "Should not be empty!")
        @Email
        String email;

        @NotBlank
        String newUsername;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ChangeUserStatusAdapter {
        @NotBlank(message = "Should not be empty!")
        @Email
        String email;

        @NotBlank
        String status;
    }
}
