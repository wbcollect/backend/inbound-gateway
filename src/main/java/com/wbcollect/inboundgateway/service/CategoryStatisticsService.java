package com.wbcollect.inboundgateway.service;

import com.wbcollect.inboundgateway.controller.CategoryStatisticsController;
import com.wbcollect.statistics.StatisticsGrpc;
import com.wbcollect.statistics.StatisticsProto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static com.wbcollect.inboundgateway.util.Utils.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class CategoryStatisticsService {
    private final StatisticsGrpc.StatisticsBlockingStub statisticsBlockingStub;

    public CategoryStatisticsController.CategoryStatisticsAdapter getCategoryStatistics(String categoryUrl, Integer days) {
        var getProductStatsRequest = StatisticsProto.CategoryStatsRequest
                .newBuilder()
                .setCategoryUrl(categoryUrl)
                .setNumberOfDays(days)
                .build();
        var categoryStatsReply = statisticsBlockingStub.getCategoryStats(getProductStatsRequest);
        log.info("Got stats array for category with url " + categoryUrl + " through gRPC call to statistics service");

        return mapCategoryStatsReplyToCategoryStatisticsAdapter(categoryStatsReply);
    }
}
