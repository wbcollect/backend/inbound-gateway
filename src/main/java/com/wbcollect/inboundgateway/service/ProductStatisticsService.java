package com.wbcollect.inboundgateway.service;

import com.wbcollect.inboundgateway.controller.ProductStatisticsController;
import com.wbcollect.statistics.StatisticsGrpc;
import com.wbcollect.statistics.StatisticsProto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static com.wbcollect.inboundgateway.util.Utils.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductStatisticsService {
    private final StatisticsGrpc.StatisticsBlockingStub statisticsBlockingStub;

    public ProductStatisticsController.ProductInfoAdapter getProductInfo(Long id) {
        var getProductInfoRequest = StatisticsProto.ProductInfoRequest
                .newBuilder()
                .setId(id)
                .build();
        var productInfoReply = statisticsBlockingStub.getProductInfo(getProductInfoRequest);
        log.info("Got info for product with id " + id + " through gRPC call to statistics service");

        return mapProductInfoReplyToProductInfoAdapter(productInfoReply);
    }


    public ProductStatisticsController.ProductStatisticsAdapter getProductStatistics(Long id, Integer days) {
        var getProductStatsRequest = StatisticsProto.ProductStatsRequest
                .newBuilder()
                .setId(id)
                .setNumberOfDays(days)
                .build();
        var productStatsReply = statisticsBlockingStub.getProductStats(getProductStatsRequest);
        log.info("Got stats array for product with id " + id + " through gRPC call to statistics service");

        return mapProductStatsReplyToProductStatisticsAdapter(productStatsReply);
    }
}
