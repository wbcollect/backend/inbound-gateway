package com.wbcollect.inboundgateway.service;

import com.wbcollect.inboundgateway.controller.UserController;
import com.wbcollect.usermanager.UserManagerGrpc;
import com.wbcollect.usermanager.UserManagerProto;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserManagerGrpc.UserManagerBlockingStub userManagerBlockingStub;

    public UserController.UserAdapter createUser(final UserController.UserAdapter userData)
            throws StatusRuntimeException {
        var request = UserManagerProto.CreateUserRequest.newBuilder()
                .setUsername(userData.getUsername())
                .setPassword(passwordEncoder.encode(userData.getPassword()))
                .setEmail(userData.getEmail())
                .build();

        var createUserReply = this.userManagerBlockingStub.createUser(request).getUser();

        log.info("Created " + userData.getEmail() + " account through gRPC call to user-manager service");
        return new UserController.UserAdapter(
                createUserReply.getId(),
                createUserReply.getUsername(),
                createUserReply.getPassword(),
                createUserReply.getEmail(),
                createUserReply.getStatus());
    }

    public UserController.UserAdapter getUser(final String email)
            throws StatusRuntimeException {
        var userRequest = UserManagerProto.UserRequest.newBuilder().setEmail(email).build();

        var userReply = this.userManagerBlockingStub.getUser(userRequest).getUser();
        log.info("Got " + email + " account info through gRPC call to user-manager service");

        return new UserController.UserAdapter(
                userReply.getId(),
                userReply.getUsername(),
                userReply.getPassword(),
                userReply.getEmail(),
                userReply.getStatus());
    }

    public UserController.UserAdapter changeUserPassword(String email, String oldPassword, String newPassword)
            throws StatusRuntimeException {
        var userRequest = UserManagerProto.UserRequest.newBuilder().setEmail(email).build();

        var userReply = this.userManagerBlockingStub.getUser(userRequest);
        if (!checkIfValidOldPassword(userReply.getUser().getPassword(), oldPassword))
            throw new StatusRuntimeException(Status.UNAUTHENTICATED);

        var request = UserManagerProto.ChangeUserRequest.newBuilder()
                .setPassword(passwordEncoder.encode(newPassword))
                .setEmail(email)
                .build();
        var changeUserReply = this.userManagerBlockingStub.changeUser(request);
        log.info("Changed password for " + email + " account through gRPC call to user-manager service");

        return new UserController.UserAdapter(
                changeUserReply.getUser().getId(),
                changeUserReply.getUser().getUsername(),
                changeUserReply.getUser().getPassword(),
                changeUserReply.getUser().getEmail(),
                changeUserReply.getUser().getStatus());
    }

    public UserController.UserAdapter changeUserName(String email, String newUsername)
            throws StatusRuntimeException {
        var request = UserManagerProto.ChangeUserRequest.newBuilder()
                .setUsername(newUsername)
                .setEmail(email)
                .build();
        var changeUserReply = this.userManagerBlockingStub.changeUser(request);
        log.info("Changed username for " + email + " account through gRPC call to user-manager service");

        return new UserController.UserAdapter(
                changeUserReply.getUser().getId(),
                changeUserReply.getUser().getUsername(),
                changeUserReply.getUser().getPassword(),
                changeUserReply.getUser().getEmail(),
                changeUserReply.getUser().getStatus());
    }

    public UserController.UserAdapter changeUserStatus(String email, String status)
            throws StatusRuntimeException {
        var request = UserManagerProto.ChangeUserRequest.newBuilder()
                .setStatus(status)
                .setEmail(email)
                .build();
        var changeUserReply = this.userManagerBlockingStub.changeUser(request);
        log.info("Changed user status for " + email + " account through gRPC call to user-manager service");

        return new UserController.UserAdapter(
                changeUserReply.getUser().getId(),
                changeUserReply.getUser().getUsername(),
                changeUserReply.getUser().getPassword(),
                changeUserReply.getUser().getEmail(),
                changeUserReply.getUser().getStatus());
    }

    public boolean checkIfValidOldPassword(final String currentPassword, final String oldPassword) {
        return passwordEncoder.matches(oldPassword, currentPassword);
    }
}
